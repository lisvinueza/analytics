WITH granular_data AS (

  SELECT * 
  FROM {{ ref('wk_fct_targets_actuals_5th_day_weekly_snapshot')}}

),

aggregate_data AS (

  SELECT * 
  FROM {{ ref('wk_fct_targets_actuals_aggregate')}}

),

unioned AS (


  SELECT 
    granular_data.*,
    TRUE AS is_granular
  FROM granular_data
  WHERE is_current_snapshot_quarter

  UNION

  SELECT 
    NULL AS actuals_targets_pk,
    NULL AS dim_crm_opportunity_id,
    dim_sales_qualified_source_id,
    dim_order_type_id,
    dim_order_type_live_id,
    dim_crm_user_hierarchy_sk,
    crm_user_business_unit,
    crm_user_sales_segment,
    crm_user_geo,
    crm_user_region,
    crm_user_area,
    crm_user_role_name,
    crm_user_role_level_1,
    crm_user_role_level_2,
    crm_user_role_level_3,
    crm_user_role_level_4,
    crm_user_role_level_5,
    crm_user_sales_segment_grouped,
    crm_user_sales_segment_region_grouped,
    NULL AS merged_crm_opportunity_id,
    NULL AS dim_crm_account_id,
    NULL AS dim_crm_person_id,
    NULL AS sfdc_contact_id,
    NULL AS record_type_id,
    NULL AS dim_crm_opp_owner_stamped_hierarchy_sk,
    NULL AS technical_evaluation_date_id,
    NULL AS ssp_id,
    NULL AS ga_client_id,

    NULL AS report_user_segment_geo_region_area_sqs_ot,
    sales_qualified_source_name,
    order_type,
    order_type_live,
    order_type_grouped,
    stage_name,
    deal_path_name,
    sales_type,
    parent_crm_account_industry,

    snapshot_date,

    NULL AS is_closed,
    NULL AS is_won,
    NULL AS is_refund,
    NULL AS is_downgrade,
    NULL AS is_swing_deal,
    NULL AS is_edu_oss,
    NULL AS is_web_portal_purchase,
    NULL AS fpa_master_bookings_flag,
    NULL AS is_sao,
    NULL AS is_sdr_sao,
    NULL AS is_net_arr_closed_deal,
    NULL AS is_new_logo_first_order,
    NULL AS is_net_arr_pipeline_created_combined,
    NULL AS is_win_rate_calc,
    NULL AS is_closed_won,
    NULL AS is_stage_1_plus,
    NULL AS is_stage_3_plus,
    NULL AS is_stage_4_plus,
    NULL AS is_lost,
    NULL AS is_open,
    NULL AS is_active,
    NULL AS is_credit,
    NULL AS is_renewal,
    NULL AS is_deleted,
    NULL AS is_excluded_from_pipeline_created_combined,
    NULL AS created_in_snapshot_quarter_deal_count,
    NULL AS is_duplicate,
    NULL AS is_contract_reset,
    NULL AS is_comp_new_logo_override,
    NULL AS is_eligible_open_pipeline_combined,
    NULL AS is_eligible_age_analysis_combined,
    NULL AS is_eligible_churn_contraction,
    NULL AS is_booked_net_arr,
    NULL AS is_abm_tier_sao,
    NULL AS is_abm_tier_closed_won,
    NULL AS primary_solution_architect,
    NULL AS product_details,
    NULL AS product_category,
    NULL AS intended_product_tier,
    NULL AS products_purchased,
    NULL AS growth_type,
    NULL AS opportunity_deal_size,
    NULL AS closed_buckets,
    calculated_deal_size,
    deal_size,
    NULL AS lead_source,
    NULL AS dr_partner_deal_type,
    NULL AS dr_partner_engagement,
    NULL AS partner_account,
    NULL AS dr_status,
    NULL AS dr_deal_id,
    NULL AS dr_primary_registration,
    NULL AS distributor,
    NULL AS influence_partner,
    NULL AS fulfillment_partner,
    NULL AS platform_partner,
    NULL AS partner_track,
    NULL AS resale_partner_track,
    NULL AS is_public_sector_opp,
    NULL AS is_registration_from_portal,
    NULL AS calculated_discount,
    NULL AS partner_discount,
    NULL AS partner_discount_calc,
    NULL AS comp_channel_neutral,

    segment_order_type_iacv_to_net_arr_ratio,
    calculated_from_ratio_net_arr,
    net_arr,
    raw_net_arr,
    created_and_won_same_quarter_net_arr_combined,
    new_logo_count,
    amount,
    recurring_amount,
    true_up_amount,
    proserv_amount,
    other_non_recurring_amount,
    arr_basis,
    arr,
    count_crm_attribution_touchpoints,
    weighted_linear_iacv,
    count_campaigns,
    probability,
    days_in_sao,
    open_1plus_deal_count,
    open_3plus_deal_count,
    open_4plus_deal_count,
    booked_deal_count,
    churned_contraction_deal_count,
    open_1plus_net_arr,
    open_3plus_net_arr,
    open_4plus_net_arr,
    booked_net_arr,
    churned_contraction_net_arr,
    calculated_deal_count,
    booked_churned_contraction_deal_count,
    booked_churned_contraction_net_arr,
    renewal_amount,
    total_contract_value,
    days_in_stage,
    calculated_age_in_days,
    days_since_last_activity,
    pre_military_invasion_arr,
    won_arr_basis_for_clari,
    arr_basis_for_clari,
    forecasted_churn_for_clari,
    override_arr_basis_clari,
    vsa_start_date_net_arr,
    cycle_time_in_days_combined,
    snapshot_day,
    snapshot_day_name,
    snapshot_fiscal_year,
    snapshot_fiscal_quarter_name,
    snapshot_fiscal_quarter_date,
    NULL AS snapshot_day_of_fiscal_quarter_normalised,
    NULL AS snapshot_day_of_fiscal_year_normalised,
    NULL AS snapshot_day_of_week,
    NULL AS snapshot_first_day_of_week,
    snapshot_week_of_year,
    NULL AS snapshot_day_of_month,
    NULL AS snapshot_day_of_quarter,
    NULL AS snapshot_day_of_year,
    snapshot_fiscal_quarter,
    NULL AS snapshot_day_of_fiscal_quarter,
    NULL AS snapshot_day_of_fiscal_year,
    snapshot_month_name,
    snapshot_first_day_of_month,
    snapshot_last_day_of_month,
    snapshot_first_day_of_year,
    snapshot_last_day_of_year,
    snapshot_first_day_of_quarter,
    snapshot_last_day_of_quarter,
    snapshot_first_day_of_fiscal_quarter,
    snapshot_last_day_of_fiscal_quarter,
    snapshot_first_day_of_fiscal_year,
    snapshot_last_day_of_fiscal_year,
    snapshot_week_of_fiscal_year,
    snapshot_month_of_fiscal_year,
    NULL AS snapshot_last_day_of_week,
    snapshot_quarter_name,
    snapshot_fiscal_quarter_name_fy,
    snapshot_fiscal_quarter_number_absolute,
    snapshot_fiscal_month_name,
    snapshot_fiscal_month_name_fy,
    NULL AS snapshot_holiday_desc,
    NULL AS snapshot_is_holiday,
    snapshot_last_month_of_fiscal_quarter,
    NULL AS snapshot_is_first_day_of_last_month_of_fiscal_quarter,
    snapshot_last_month_of_fiscal_year,
    NULL AS snapshot_is_first_day_of_last_month_of_fiscal_year,
    snapshot_days_in_month_count,
    snapshot_week_of_month_normalised,
    snapshot_week_of_fiscal_quarter_normalised,
    NULL AS snapshot_is_first_day_of_fiscal_quarter_week,
    NULL AS snapshot_days_until_last_day_of_month,
    current_date_actual,
    current_fiscal_year,
    current_first_day_of_fiscal_year,
    current_fiscal_quarter_name_fy,
    current_first_day_of_month,
    current_first_day_of_fiscal_quarter,
    current_day_of_month,
    current_day_of_fiscal_quarter,
    current_day_of_fiscal_year,

    NULL AS created_date,
    NULL AS created_month,
    NULL AS created_fiscal_quarter_date,
    NULL AS created_fiscal_quarter_name,
    NULL AS created_fiscal_year,
    NULL AS sales_accepted_date,
    NULL AS sales_accepted_month,
    NULL AS sales_accepted_fiscal_quarter_date,
    NULL AS sales_accepted_fiscal_quarter_name,
    NULL AS sales_accepted_fiscal_year,
    NULL AS close_date,
    NULL AS close_month,
    NULL AS close_fiscal_quarter_date,
    NULL AS close_fiscal_quarter_name,
    NULL AS close_fiscal_year,
    NULL AS stage_0_pending_acceptance_date,
    NULL AS stage_0_pending_acceptance_month,
    NULL AS stage_0_pending_acceptance_fiscal_quarter_date,
    NULL AS stage_0_pending_acceptance_fiscal_quarter_name,
    NULL AS stage_0_pending_acceptance_fiscal_year,
    NULL AS stage_1_discovery_date,
    NULL AS stage_1_discovery_month,
    NULL AS stage_1_discovery_fiscal_quarter_date,
    NULL AS stage_1_discovery_fiscal_quarter_name,
    NULL AS stage_1_discovery_fiscal_year,
    NULL AS stage_2_scoping_date,
    NULL AS stage_2_scoping_month,
    NULL AS stage_2_scoping_fiscal_quarter_date,
    NULL AS stage_2_scoping_fiscal_quarter_name,
    NULL AS stage_2_scoping_fiscal_year,
    NULL AS stage_3_technical_evaluation_date,
    NULL AS stage_3_technical_evaluation_month,
    NULL AS stage_3_technical_evaluation_fiscal_quarter_date,
    NULL AS stage_3_technical_evaluation_fiscal_quarter_name,
    NULL AS stage_3_technical_evaluation_fiscal_year,
    NULL AS stage_4_proposal_date,
    NULL AS stage_4_proposal_month,
    NULL AS stage_4_proposal_fiscal_quarter_date,
    NULL AS stage_4_proposal_fiscal_quarter_name,
    NULL AS stage_4_proposal_fiscal_year,
    NULL AS stage_5_negotiating_date,
    NULL AS stage_5_negotiating_month,
    NULL AS stage_5_negotiating_fiscal_quarter_date,
    NULL AS stage_5_negotiating_fiscal_quarter_name,
    NULL AS stage_5_negotiating_fiscal_year,
    NULL AS stage_6_awaiting_signature_date_date,
    NULL AS stage_6_awaiting_signature_date_month,
    NULL AS stage_6_awaiting_signature_date_fiscal_quarter_date,
    NULL AS stage_6_awaiting_signature_date_fiscal_quarter_name,
    NULL AS stage_6_awaiting_signature_date_fiscal_year,
    NULL AS stage_6_closed_won_date,
    NULL AS stage_6_closed_won_month,
    NULL AS stage_6_closed_won_fiscal_quarter_date,
    NULL AS stage_6_closed_won_fiscal_quarter_name,
    NULL AS stage_6_closed_won_fiscal_year,
    NULL AS stage_6_closed_lost_date,
    NULL AS stage_6_closed_lost_month,
    NULL AS stage_6_closed_lost_fiscal_quarter_date,
    NULL AS stage_6_closed_lost_fiscal_quarter_name,
    NULL AS stage_6_closed_lost_fiscal_year,
    NULL AS subscription_start_date,
    NULL AS subscription_start_month,
    NULL AS subscription_start_fiscal_quarter_date,
    NULL AS subscription_start_fiscal_quarter_name,
    NULL AS subscription_start_fiscal_year,
    NULL AS subscription_end_date,
    NULL AS subscription_end_month,
    NULL AS subscription_end_fiscal_quarter_date,
    NULL AS subscription_end_fiscal_quarter_name,
    NULL AS subscription_end_fiscal_year,
    NULL AS sales_qualified_date,
    NULL AS sales_qualified_month,
    NULL AS sales_qualified_fiscal_quarter_date,
    NULL AS sales_qualified_fiscal_quarter_name,
    NULL AS sales_qualified_fiscal_year,
    NULL AS last_activity_date,
    NULL AS last_activity_month,
    NULL AS last_activity_fiscal_quarter_date,
    NULL AS last_activity_fiscal_quarter_name,
    NULL AS last_activity_fiscal_year,
    NULL AS sales_last_activity_date,
    NULL AS sales_last_activity_month,
    NULL AS sales_last_activity_fiscal_quarter_date,
    NULL AS sales_last_activity_fiscal_quarter_name,
    NULL AS sales_last_activity_fiscal_year,
    NULL AS technical_evaluation_date,
    NULL AS technical_evaluation_month,
    NULL AS technical_evaluation_fiscal_quarter_date,
    NULL AS technical_evaluation_fiscal_quarter_name,
    NULL AS technical_evaluation_fiscal_year,
    NULL AS arr_created_date,
    NULL AS arr_created_month,
    NULL AS arr_created_fiscal_quarter_date,
    NULL AS arr_created_fiscal_quarter_name,
    NULL AS arr_created_fiscal_year,
    NULL AS pipeline_created_date,
    NULL AS pipeline_created_month,
    NULL AS pipeline_created_fiscal_quarter_date,
    NULL AS pipeline_created_fiscal_quarter_name,
    NULL AS pipeline_created_fiscal_year,
    NULL AS net_arr_created_date,
    NULL AS net_arr_created_month,
    NULL AS net_arr_created_fiscal_quarter_date,
    NULL AS net_arr_created_fiscal_quarter_name,
    NULL AS net_arr_created_fiscal_year,

    NULL AS deals_daily_target,
    deals_monthly_target,
    deals_quarterly_target,
    deals_wtd_target,
    deals_mtd_target,
    deals_qtd_target,
    deals_ytd_target,
    NULL AS mql_daily_target,
    mql_monthly_target,
    mql_quarterly_target,
    mql_wtd_target,
    mql_mtd_target,
    mql_qtd_target,
    mql_ytd_target,
    NULL AS net_arr_daily_target,
    net_arr_monthly_target,
    net_arr_quarterly_target,
    net_arr_wtd_target,
    net_arr_mtd_target,
    net_arr_qtd_target,
    net_arr_ytd_target,
    NULL AS net_arr_company_daily_target,
    net_arr_company_monthly_target,
    net_arr_company_quarterly_target,
    net_arr_company_wtd_target,
    net_arr_company_mtd_target,
    net_arr_company_qtd_target,
    net_arr_company_ytd_target,
    NULL AS net_arr_pipeline_created_daily_target,
    net_arr_pipeline_created_monthly_target,
    net_arr_pipeline_created_quarterly_target,
    net_arr_pipeline_created_wtd_target,
    net_arr_pipeline_created_mtd_target,
    net_arr_pipeline_created_qtd_target,
    net_arr_pipeline_created_ytd_target,
    NULL AS new_logos_daily_target,
    new_logos_monthly_target,
    new_logos_quarterly_target,
    new_logos_wtd_target,
    new_logos_mtd_target,
    new_logos_qtd_target,
    new_logos_ytd_target,
    NULL AS stage_1_opportunities_daily_target,
    stage_1_opportunities_monthly_target,
    stage_1_opportunities_quarterly_target,
    stage_1_opportunities_wtd_target,
    stage_1_opportunities_mtd_target,
    stage_1_opportunities_qtd_target,
    stage_1_opportunities_ytd_target,
    NULL AS total_closed_daily_target,
    total_closed_monthly_target,
    total_closed_quarterly_target,
    total_closed_wtd_target,
    total_closed_mtd_target,
    total_closed_qtd_target,
    total_closed_ytd_target,
    NULL AS trials_daily_target,
    trials_monthly_target,
    trials_quarterly_target,
    trials_wtd_target,
    trials_mtd_target,
    trials_qtd_target,
    trials_ytd_target,
    NULL AS is_created_in_snapshot_week,
    NULL AS is_close_in_snapshot_week,
    NULL AS is_arr_created_in_snapshot_week,
    NULL AS is_net_arr_created_in_snapshot_week,
    NULL AS is_pipeline_created_in_snapshot_week,
    NULL AS is_sales_accepted_in_snapshot_week,
    NULL AS is_closed_won_in_snapshot_week,
    NULL AS is_closed_lost_in_snapshot_week,
    created_arr_in_snapshot_week,
    created_net_arr_in_snapshot_week,
    created_deal_count_in_snapshot_week,
    closed_net_arr_in_snapshot_week,
    closed_deal_count_in_snapshot_week,
    closed_new_logo_count_in_snapshot_week,
    closed_cycle_time_in_snapshot_week,
    booked_net_arr_in_snapshot_week,
    pipeline_created_in_snapshot_week,
    calculated_deal_count_in_snapshot_week,
    open_pipeline_in_snapshot_week,
    closed_lost_opps_in_snapshot_week,
    closed_won_opps_in_snapshot_week,
    closed_opps_in_snapshot_week,
    NULL AS is_current_snapshot_quarter,
    FALSE AS is_granular
  FROM aggregate_data
  WHERE NOT is_current_snapshot_quarter


)

SELECT *
FROM unioned
